#include <stdio.h>

int main(int argc, char** argv) {
	int number_entries, i, j;
	printf("How many numbers would you like me to sort?\n");
	scanf("%d", &number_entries);
	int lst[number_entries];

	int temp;
	for (i=0; i<number_entries; i++){
		printf("What is the next number: \n");
		scanf("%d", &temp);
		lst[i] = temp;
	}

	int current_num;
	int low;
	int min;
	for (i=0; i<number_entries-1; i++){
		current_num = *(lst+i);
		min = i;
		for (j=i+1; j<number_entries; j++){
			low = *(lst+j);
			if (low < *(lst+min)){
				min = j;
			}
		}
		//took a long time and had to reference Kernighan & Ritchie to realize the two indexing approaches exactly same
		int temp = *(lst+min);
		*(lst+min) = *(lst+i);
		*(lst+i) = temp;
	}

	for (i=0; i<number_entries; i++){
		int a = lst[i];
		printf("%d ", a);
	}
	return 0;
}

