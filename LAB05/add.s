
.global main

.output: .string "Summing 5 and 42 is : %d\n"
	#.asciz == .string
main:

	movq $5, %rcx
	movq $42, %rbx			#we expect to get 47

	addq %rcx, %rbx

				#first move for printing
	
	movq $.output, %rdi	#dereferencing output?
	movq %rbx, %rsi		#documentation states for printf, rsi and rdi need arguments 
	xor %rax, %rax
	call printf 

				#after fn call, cant trust anything where it says it is, need to redo (wont remain after 				#fn call
				#can push to stack before, then pop after rcx, then rcx register is safe from printf, 					#d stays as 5 instead of return to 0
				#rax is where output goes??
