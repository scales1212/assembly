#Assignment 5
#CS330
#Stephen Scales

.data 
.output: .string "%i\n%i\n%i\n"

.text
.global main

times:
	#rdi is A
	#rsi is B
	movq %rdi, %rax
	imulq $2, %rax
	ret

divideadd:
	movq %rdi, %rax
	cdq
	movq %rsi, %rbx
	idiv %rbx
	imulq %rdi, %rsi
	leaq (%rsi, %rax), %rax
	ret

minusplus:
	movq %rdi, %rax
	subq %rsi, %rax
	addq %rdi, %rsi
	leaq (%rsi, %rax), %rax
	ret
main:
	mov $15, %rdi
	mov $4, %rsi
	
	call times
	push %rax #push to save for later
	xor %rax, %rax

	call divideadd
	mov %rax, %rdx
	xor %rax, %rax

	call minusplus
	mov %rax, %rcx
	xor %rax, %rax

	popq %rsi 
	mov $.output, %rdi
	call printf
