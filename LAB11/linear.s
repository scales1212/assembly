.data 
output: .string "Index of largest element: %d\n"
arr: .quad 1,2,3,4,5,6,7,8,9,10
arrlen: .quad 10

.text 
.global main

linear:
	#rdi is pointer to array
	#rsi is length of array
	#rdx is target number
	xor %rax, %rax

_argmax:
	cmp (%rdi, %rax, 8), %rdx
	je _argmaxret
	
	inc %rax
	cmp %rsi, %rax
	jl _argmax
	
	mov $-1, %rax
	ret

_argmaxret:
	ret

main:
	mov $arr, %rdi
	mov arrlen, %rsi #value not pointer
	mov $6, %rdx

	call linear

	mov $output, %rdi
	mov %rax, %rsi
	xor %rax, %rax

	call printf 	
	
