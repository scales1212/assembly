.data
output: .string "%i "
newline: .string "\n"
arr: .quad 8, 7, 5, 4, 3, 1
arrlen: .quad 6

.text
.global main

switch:
	mov %rbx, (%rdi, %rax, 8)
	mov %rcx, (%rdi, %rdx, 8)
	inc %rdx
	inc %rax
	cmp %rdx, %r12
	jg _inner
	ret
inner:
	#rdi pointer to array
	#rsi len of array
	#rdx is next val
	#rax current index
	#r12 is array len in inner loop
	xor %rax, %rax
	mov $1, %rdx
_inner:
	mov (%rdi, %rdx, 8), %rbx #temp reg for compare	
	mov (%rdi, %rax, 8), %rcx #temp reg for compare
	cmp %rcx, %rbx
	jl switch
	
	inc %rdx
	inc %rax
	cmp %rdx, %r12
	jg _inner
	ret
	
outer:
	#r12 keeps the inner arraylen
	#r11 iterates up as index for outer
	xor %rax, %rax
	xor %r11, %r11
	mov %rsi, %r12
_outer:
	call inner
	inc %r11
	dec %r12
	cmp %r11, %rsi
	jg _outer
	ret

#previously implemented print array
printArr:
	xor %rax, %rax
	
_printArr:
	push %rdi
	push %rsi
	push %rax

	mov (%rdi, %rax, 8), %rsi
	mov $output, %rdi
	xor %rax, %rax
	call printf

	pop %rax
	pop %rsi
	pop %rdi

	inc %rax
	cmp %rsi, %rax
	jl _printArr
	mov $newline, %rdi
	xor %rax, %rax
	call printf

	ret 
main:
	mov $arr, %rdi 
	mov (arrlen), %rsi
	call outer

	call printArr	
