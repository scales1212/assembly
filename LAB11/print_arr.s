.data
    arr: .quad 17, 25, 13, 15, 3, 99, 4, 0, 1
    arrlen: .quad 9
    output: .string "%i "
    newline: .string "\n"

.text
    .global main


printArr:
    xor %rax, %rax  # zero out index reg

_printArr:
    push %rdi  # protect some regs that printf breaks
    push %rsi
    push %rax

    movq (%rdi, %rax, 8), %rsi  # get an element, into second param register
    movq $output, %rdi  # move str into first param register
    xor %rax, %rax  # no floating point regs
    call printf

    pop %rax  # restore regs
    pop %rsi
    pop %rdi

    inc %rax  # next index
    cmpq %rsi, %rax  # make sure we haven't gotten to len yet
    jl _printArr  # if rax <= rsi

    # else
    movq $newline, %rdi  # move str into first param register
    xor %rax, %rax  # no floating point regs
    call printf

    ret  

main:
    mov $arr, %rdi
    mov (arrlen), %rsi
    call printArr
