.data
arr: .quad 8, 8, 8, 8, 8 #sum 40
.text
output: .string "sum of array : %d\n"
.global main

sumarr:
	add (%rdi, %rsi,8), %rax 
	dec %rsi
	cmp %rdx, %rsi
	jge sumarr
	ret
	 
main:
	mov $arr, %rdi #first arg
	mov $5, %rsi #length  
	dec %rsi
	mov $0, %rdx
	mov $0, %rax
	call sumarr
	
	mov $output, %rdi
	mov %rax, %rsi
	xor %rax, %rax
	call printf
	
