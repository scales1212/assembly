/*Assignment 7: takes in three sides of a triangle and returns truthy that indicates whether valid triangle or not*/

.data
.output: .string "The truthy val is : %i\n"

.text 
.global main

//_test:
//	push $1
test:
	mov %rdi, %rax
	add %rsi, %rax
	cmp %rdx, %rax
	jl fail

	mov %rdi, %rax
	mov %rdx, %rdi
	mov %rsi, %rdx
	mov %rax, %rsi

	mov %rdi, %rax
	add %rsi, %rax
	cmp %rdx, %rax
	jl fail

	mov %rdi, %rax
	mov %rdx, %rdi
	mov %rsi, %rdx
	mov %rax, %rsi

	mov %rdi, %rax
	add %rsi, %rax
	cmp %rdx, %rax
	jl fail 
	mov $1, %rsi
	ret	
	

fail:
	xor %rax, %rax
	mov $0, %rsi
	mov $.output, %rdi
	call printf
	call exit
main: 
//	mov $3, %rdi ##number of iterations
	mov $1, %rdi #a
	mov $1, %rsi #b
	mov $5, %rdx #c
	
	call test
	xor %rax, %rax
	mov $.output, %rdi
	call printf

	
