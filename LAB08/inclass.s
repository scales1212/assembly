/*in class work for creating pyramid of stars*/

.data 
.star: .string "*"
.line: .string "\n"

.text
.global main 

printstars:
	push %rdi
	mov $.star, %rdi
	xor %rax, %rax
	call printf
	pop %rdi

	dec %rdi
	cmp $0, %rdi
	jg printstars

	mov $.line, %rdi
	xor %rax, %rax
	call printf
	ret

printstarpyr:
	mov %rdi, %rsi 	#rsi max num stars to print stars to print 
	mov $1, %rdi	#rdi is current number of starts to print 

_printstarpyr:

	push %rsi
	push %rdi
	call printstars 
	pop %rdi
	pop %rsi
	inc %rdi
	cmp %rsi, %rdi	#check if we are done printing 
	jle _printstarpyr
	ret 
	
main:
	mov $5, %rdi
	
	call printstarpyr
	
