/*Lab ex for finding max in list */

.data 
#name: .size item, item, item #syntax
arr: .quad 7, 4, 9, 6, 3 #values
.large: .string "Index of largest emelment:  %ld\n"

.text 
.global main

argmax:
	mov $0, %rax
	mov $1, %rbx #current index

_argmax:
	mov (%rdi, %rbx, 8), %rcx
	cmp (%rdi, %rax, 8), %rcx
	cmovg %rbx, %rax

	inc %rbx
	cmp %rbx, %rsi
	jge _argmax
	ret

main:
	mov $arr, %rdi
	mov $5, %rsi
	call argmax
	#return index in rax
	
	mov $output, %rdi
	call printf

