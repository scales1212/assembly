/*factorial fn up to n, a given number hard coded for Assignment 6(6 is the number in this case)*/

.data
.output: .string "%i\n"

.text
.global main 

_comp:
	push $1 #starting with 1 and iterating up to 6
	mov $1, %rsi #counter
	
comp:
	pop %rax
	inc %rsi
	imul %rsi, %rax #multiply total by inc counter value until after reaches 6
	push %rax
	cmp %rsi, %rdi 
	jg comp
	pop %rsi 
	ret

main:
	mov $6, %rdi #find factorial of 6
	call _comp
	xor %rax, %rax
	mov $.output, %rdi
	call printf
	
		
