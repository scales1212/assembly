/*Assignment 6 is prime that returns 0 for false and 1 for true*/

.data
.output: .string "%i\n"

.text
.global main 
	
prime:
	movq %rdi, %rax 
	movq %rsi, %rbx
	idivq %rbx #remainder of rdi/rsi goes to rdx
	movq %rax, %rdi 
	movq %rbx, %rsi 
	mov $1, %rax #if less than 1, must be zero so remainder zero and not prime 
	inc %rsi
	cmp %rax, %rdx
	jge prime
	jl no
	je yes
	ret
no:
	xor %rsi, %rsi
	ret
yes:
	mov $1, %rsi
	ret
main:
	mov $5, %rdi #5 is the value to check if prime or not 
	mov $2, %rsi #start with 2 bc 1 doesn't help as a divisor when finding prime
	call prime
	mov $.output, %rsi
	call printf 
