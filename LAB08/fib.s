/*fib function based on n as input that is hard coded, for assignment 6*/
/*input is 9, which the 9th fib number is 23...starts with 0 and 1 as 1st and second fib */
.data
.output: .string "%i \n"

.text
.global main


_iterate:
	push $0 #first val in fib
	push $1 #second value in fib
	mov $2, %rsi #counter 
iterate:	
	pop %rax
	pop %rdx #lower value
	addq %rax, %rdx
	push %rax #lower value
	push %rdx
	inc %rsi
	cmp %rsi, %rdi #if counter less than goal fib number, run again
	jg iterate
	pop %rsi
	pop %rdx
	ret

main:
	movq $9, %rdi #9th fib number
	call _iterate

	movq $.output, %rdi
	xor %rax, %rax
	call printf 
	 
	
