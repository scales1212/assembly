#Assignment 10, binary search 

.data 
output: .string "%i\n"
noout: .string "sorry but %i wasn't found\n"
arr: .quad 1,3,5,7,9
arrlen: .quad 5
search: .quad 7

.text
.global main

binary:
	xor %rbx, %rbx   #rbx is start ptr
	xor %rdx, %rdx
_binary:
	#rdx will be mid
	cmp %rbx, %rsi
	jle none
	mov %rsi, %rax
	sub %rbx, %rax #get new len
	inc %rax
	cqto
	push %rbx
	mov $2, %rbx
	idiv %rbx
	pop %rbx
	
	add %rbx, %rdx
//	inc %rdx
	cmp %rdx, %rsi # second compare to make sure dont index out of array
	jl none
	mov (%rdi, %rdx, 8), %rax #mid value
	cmp %rax, %rcx 
	jg greater
	jl less
	ret
	

greater:
	mov %rdx, %rbx
	inc %rbx
	jmp _binary 	
less:
	mov %rdx, %rsi
	jmp _binary

none:
	mov %rcx, %rsi
	mov $noout, %rdi
	xor %rax, %rax
	call printf
	call exit

main:
	mov $arr, %rdi #array
	mov arrlen, %rsi # length
	mov search, %rcx #search for val
	call binary
	
	mov %rdx, %rsi
	mov $output, %rdi
	xor %rax, %rax
	call printf
