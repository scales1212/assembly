#include <stdio.h>

int findMax(int *arr, int size);

int main(){
	int i, maxi, size;
	printf("How many integers would you like in the list: ");
	scanf("%d",&size);

	int arr[size];
	for (i=0; i<size; i++){
		printf("Whats the next number to add: ");
		scanf("%d",(arr+i));
	}

	maxi = findMax(arr, size);
	printf("The highest integer in the list given is : %d\n", maxi);

	return 0;
}

int findMax(int *arr, int size){
	int i, maxi;
	maxi = 0;
	for (i=0; i<size; i++){
		if (*(arr+i) > *(arr+maxi)){
			maxi = i;
		}
	}
	return maxi;
}
