	.file	"MaxIndex.c"
	.section	.rodata			
	.align 8
.LC0:
	.string	"How many integers would you like in the list: "
.LC1:
	.string	"%d"
	.align 8
.LC2:
	.string	"Whats the next number to add: "
	.align 8
.LC3:
	.string	"The highest integer in the list given is : %d\n"
	.text
	.globl	main
	.type	main, @function
main:
.LFB0:
	.cfi_startproc						#starting program
	pushq	%rbp						#pushes previous (before this program ran) to stack
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp					#pushes pointer to current frame to stack
	.cfi_def_cfa_register 6
	pushq	%r15						#pushes callee-saved register to stack 
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx						#pushes base pointer to stack
	subq	$56, %rsp					#moves pointer back to the bottom of program stack
	.cfi_offset 15, -24					#value of register 15 has offset of 24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax					#moves index 40 in fs to rax register			
	movq	%rax, -56(%rbp)					#then moves previous fs to rbp on stack
	xorl	%eax, %eax					#sets eax to zero
	movq	%rsp, %rax					
	movq	%rax, %rbx
	movl	$.LC0, %edi					#moves first string into edi register 
	movl	$0, %eax					
	call	printf						#calls printf on first string .LCO
	leaq	-84(%rbp), %rax					#moves pointer back 56 and pushes it to rax
	movq	%rax, %rsi
	movl	$.LC1, %edi					#moves .LC1 into edi register
	movl	$0, %eax
	call	__isoc99_scanf					#calls scanf for number of int in array
	movl	-84(%rbp), %eax
	movslq	%eax, %rdx					#moves scanned value to rdx accounting for sign bit
	subq	$1, %rdx
	movq	%rdx, -72(%rbp)		
	movslq	%eax, %rdx
	movq	%rdx, %r14					#moves scanned value to r14 on stack
	movl	$0, %r15d
	movslq	%eax, %rdx
	movq	%rdx, %r12
	movl	$0, %r13d					#adds 0 as first input in each register location
	cltq							
	salq	$2, %rax
	leaq	3(%rax), %rdx
	movl	$16, %eax
	subq	$1, %rax
	addq	%rdx, %rax
	movl	$16, %ecx
	movl	$0, %edx
	divq	%rcx
	imulq	$16, %rax, %rax
	subq	%rax, %rsp
	movq	%rsp, %rax
	addq	$3, %rax
	shrq	$2, %rax
	salq	$2, %rax					#all previous assembly for moving sapce onto stack for inputs
	movq	%rax, -64(%rbp)
	movl	$0, -80(%rbp)
	jmp	.L2
.L3:
	movl	$.LC2, %edi					#moves string that asks for next integer into edi
	movl	$0, %eax
	call	printf						#prints edi 
	movq	-64(%rbp), %rax
	movl	-80(%rbp), %edx
	movslq	%edx, %rdx
	salq	$2, %rdx
	addq	%rdx, %rax
	movq	%rax, %rsi
	movl	$.LC1, %edi					#moves scan into edi 
	movl	$0, %eax
	call	__isoc99_scanf					#scans user input
	addl	$1, -80(%rbp)					
.L2:
	movl	-84(%rbp), %eax
	cmpl	%eax, -80(%rbp)
	jl	.L3						#loop for reading in and assigning int array values
	movl	-84(%rbp), %edx
	movq	-64(%rbp), %rax
	movl	%edx, %esi
	movq	%rax, %rdi
	call	findMax						#calls find max
	movl	%eax, -76(%rbp)					#moves returned value to rbp register
	movl	-76(%rbp), %eax
	movl	%eax, %esi
	movl	$.LC3, %edi					#moves string to edi for printed statement
	movl	$0, %eax
	call	printf						#prints the string and value
	movl	$0, %eax		
	movq	%rbx, %rsp
	movq	-56(%rbp), %rcx
	xorq	%fs:40, %rcx					#zeros out rcx
	je	.L5
	call	__stack_chk_fail
.L5:
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp						#removes registries from stack
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE0:
	.size	main, .-main
	.globl	findMax
	.type	findMax, @function
findMax:
.LFB1:
	.cfi_startproc
	pushq	%rbp						#moves pointer to stack
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp				
	.cfi_def_cfa_register 6
	movq	%rdi, -24(%rbp)
	movl	%esi, -28(%rbp)
	movl	$0, -4(%rbp)
	movl	$0, -8(%rbp)					#load registers and send for comparison L7
	jmp	.L7
.L9:
	movl	-8(%rbp), %eax					
	cltq
	leaq	0(,%rax,4), %rdx
	movq	-24(%rbp), %rax
	addq	%rdx, %rax
	movl	(%rax), %edx
	movl	-4(%rbp), %eax					
	cltq							#moves first value into edx
	leaq	0(,%rax,4), %rcx
	movq	-24(%rbp), %rax
	addq	%rcx, %rax
	movl	(%rax), %eax					#moves second value into eax
	cmpl	%eax, %edx					#compares eax and edx values 
	jle	.L8
	movl	-8(%rbp), %eax
	movl	%eax, -4(%rbp)					#moves value to pointer for return
.L8:
	addl	$1, -8(%rbp)					
.L7:
	movl	-8(%rbp), %eax					#loads first number into eax
	cmpl	-28(%rbp), %eax				
	jl	.L9						#calls L9 for comparison in the loop
	movl	-4(%rbp), %eax
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1:
	.size	findMax, .-findMax
	.ident	"GCC: (Ubuntu 5.4.0-6ubuntu1~16.04.11) 5.4.0 20160609"
	.section	.note.GNU-stack,"",@progbits
