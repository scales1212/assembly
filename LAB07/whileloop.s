#while loop for iterating with while loop and adding x, y
.data
.output: .string "%i \n"

.text 
.global main
addif:
	#rdi is x
	#rsi is y
	xor %rax, %rax #clearing rax bc of loop, could have junk there 
	movq %rdi, %rax
	addq %rsi, %rax

	cmpq $15, %rax #literal needs to be first!
	jle increment
	ret	
increment:		#moved into new call so not incrementing if done 
	inc %rdi
	inc %rsi
	jmp addif
main:
	mov $6, %rsi
	mov $3, %rdi
	call addif
	#now %rax has result

	mov %rax, %rsi
	mov $.output, %rdi
	xor %rax, %rax
	call printf
