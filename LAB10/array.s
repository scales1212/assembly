.data
arr: .quad 2, 3, 4, 5 #array
arrlen: .quad 4
.text
output: .string "array val: %d\n"
.global main

iterator:
	push %rsi
	push %rdi
	push %rbx
	mov (%rdi, %rbx, 8), %rsi
	call printer
	pop %rbx
	pop %rdi
	pop %rsi

	inc %rbx
	cmp %rbx, %rsi
	jne iterator
	ret

printer:
	xor %rax, %rax
	mov $output, %rdi
	call printf 
	ret

main:
	mov $arr, %rdi
	mov (arrlen), %rsi
	mov $0, %rbx
	call iterator
	call exit 
