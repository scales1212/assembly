.data

.text
output: .string "prime factor : %d\n"
.global main 


nprime: 

	inc %rsi
	
newprime:
	# rdi is int, rsi is count
	mov %rsi, %rax
	imul %rsi
	cmp %rax, %rdi
	jl exitprime

	mov %rdi, %rax
	cqto
	mov %rsi, %rbx
	idiv %rbx
	cmp $0, %rdx
	jne nprime
	mov %rax, %rdi
	
	push %rdi 
	push %rsi 

	call printint
	
	pop %rsi
	pop %rdi 

	jmp newprime
	ret 
exitprime:
	xor %rax, %rax
	mov %rdi, %rsi
	mov $output, %rdi
	call printf 
	call exit
printint:
	xor %rax, %rax
	mov $output, %rdi

	call printf

	ret 	
main:
	mov $954, %rdi #value 
	mov $2, %rsi
	//mov $0, %r12
	jmp newprime
	
	
