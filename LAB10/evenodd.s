.data

.result: .string "Is %d odd? %d \n"

arr: .quad 1,2,3,4,5,6,7,8
arrlen: .quad 8


.text
.global main

isOddOverArr:
	xor %rbx, %rbx

_isOddOverArr:
	push %rdi
	push %rbx
	mov (%rdi, %rbx, 8), %rdi
	call isOdd
	pop %rbx
	pop %rdi
	
	call printparity
	inc %rbx
	cmp %rsi, %rbx
	jl _isOddOverArr
	ret 
 
isOdd:
	#pop %rax
	mov %rdi, %rax
	cqto #extends sign rax to rdx 
	mov $2, %rbx
	idiv %rbx
	#mov %rdx, %rax
	ret
	
printparity:
	push %rdi
	
main:
	mov $arr, %rdi
	mov (arrlen), %rsi #deref to get val
	call isOddOVerArr
